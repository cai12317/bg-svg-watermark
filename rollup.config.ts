import {defineConfig} from 'rollup'
import typescript from '@rollup/plugin-typescript'
import {terser} from "rollup-plugin-terser";
import babel from "@rollup/plugin-babel";

export default defineConfig({
    input: 'src/index.ts',
    output: [
        {
            format: 'iife',
            name: 'Watermark',
            file: 'dist/bg-svg-watermark.js',
        },
        {
            format: 'es',
            file: './dist/bg-svg-watermark.es.js',
        },
        {
            format: 'cjs',
            file: './dist/bg-svg-watermark.common.js',
        },
    ],
    plugins: [
        typescript({
            sourceMap: false,
            tsconfig: './tsconfig.json',
        }),
        babel({
            exclude: "node_modules/**",
            extensions: ['.js', '.ts'],
        }),
        terser()
    ]
})
