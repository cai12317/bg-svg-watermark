# bg-svg-watermark
水印生成工具，基于 css 的 background-image 对 svg 的原生支持，原理是通过文本生成svg，在 background-image 中直接用 url 引入，利用 background-image 默认的 repeat ，创建全屏水印

## 安装
npm install bg-svg-watermark --save

## 使用
``` javascript
import {Watermark} from "bg-svg-watermark";

const watermark = new Watermark()

const options = {
    // 旋转角度 默认值：45
    rotate: '45',
    // 文本透明度 默认值：0.1
    opacity: '0.1',
    // 文本大小 默认值：16
    fontSize: '16',
    // 文本加粗 默认值：normal
    fontWeight: 'normal',
    // 文本距离外边的 padding 默认值：20
    padding: '20',
    // 文本间距 默认值：8
    space: '8',
    // 文本颜色 使用16进制颜色会渲染不出来，原因未知 默认值：rgb(0,0,0)
    color: 'rgb(0,0,0)',
}

watermark.render(['12312', '1254555'], options)

// 移除水印
// watermark.removeWatermark()

```
