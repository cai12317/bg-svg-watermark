export declare interface TextDomOptions {
    // 文本大小 默认值：16
    fontSize?: string
    // 文本加粗 默认值：normal
    fontWeight?: string
}

export declare interface WatermarkOptions extends TextDomOptions{
    // 文本透明度 默认值：0.1
    opacity?: string
    // 旋转角度 默认值：45
    rotate?: string
    // 文本距离外边的 padding 默认值：20
    padding?: number
    // 文本间距 默认值：8
    space?: number
    // 文本颜色 使用16进制颜色会渲染不出来，原因未知 默认值：rgb(0,0,0)
    color?: string
}

/** 获取文本渲染后的尺寸
 *
 * @param text 文本
 * @param textDomOptions 相关配置
 */
export declare interface getTextSize {
    (text: string, textDomOptions?: TextDomOptions): DOMRect
}

/**
 * 水印类
 */
export declare class Watermark {
    // 渲染配置
    watermarkOption?: WatermarkOptions
    // 渲染文本
    watermarkText?: string | string[]
    // 唯一的节点id
    watermarkId?: string
    // 监听器
    observer?: MutationObserver
    // 水印节点
    watermarkDom?: Element

    /** 创建水印，并且渲染到 body 下
     *
     * @param watermarkText
     * @param watermarkOption
     */
    render(watermarkText: string | string[], watermarkOption?: WatermarkOptions): void

    /** 初始化响应器
     *  使用 MutationObserver 进行简单的防破解处理
     */
    initObserver(): void

    /** 获取文本渲染后的尺寸
     *
     * @param text
     * @param watermarkOption
     */
    getTextSize(text: string, watermarkOption?: WatermarkOptions): DOMRect

    /**
     * 创建水印dom的唯一id
     */
    createDomId(): string

    /**
     * 重置水印
     */
    resetWatermark(watermarkText: string | string[]): void

    /**
     * 移除水印
     */
    removeWatermark(): void
}
