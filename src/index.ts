import { getTextSize } from './util'
import Watermark from './watermark'

export {
    Watermark,
    getTextSize
}
