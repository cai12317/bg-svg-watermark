import {TextDomOptions} from "../types";

/** 获取文本渲染后的尺寸
 *
 * @param text 文本
 * @param textDomOptions 相关配置
 */
export function getTextSize(text: string, textDomOptions?: TextDomOptions) {
    const {
        fontSize = '16px',
        fontWeight = 'normal',
    } = textDomOptions || {}

    const span = document.createElement('span')
    span.innerText = text
    span.style.fontSize = fontSize
    span.style.fontWeight = fontWeight
    span.style.fontFamily = "'Courier New', Consolas, monospace"
    document.body.appendChild(span)
    const rect = span.getBoundingClientRect()
    document.body.removeChild(span)
    return rect
}

const str = '1234567890qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM_'

/** 创建随机 id
 *
 * @param len
 */
export function randomId(len: number = 12) {
    const arr = []
    const max = str.length + 1
    for (let i = 0; i < len; i++) {
        arr.push(str[Math.floor((Math.random() * max))])
    }
    return arr.join('')
}
